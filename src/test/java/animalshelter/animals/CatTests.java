package animalshelter.animals;

import animalshelter.enums.Gender;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


class CatTests {

    @Test
    void CatConstructor_works(){
        //Arrange

        //Act
        Cat cat = new Cat("Jan", Gender.Male, "");

        //Assert
        Assertions.assertEquals("Jan", cat.getName());
        Assertions.assertEquals(Gender.Male, cat.getGender());
        Assertions.assertEquals("", cat.getBadHabits());
    }

    @Test
    void getPrice_OnANewCat_ReturnsZero(){
        //Arrange
        Cat cat = new Cat("Jan", Gender.Male, "");

        //Act
        double price = cat.getPrice();

        //Assert
        Assertions.assertEquals(0, price);
    }

    @Test
    void getPrice_AfterSettingThePriceOnACat_ReturnsTheNewPrice(){
        //Arrange
        Cat cat = new Cat("Jan", Gender.Male, "");

        //Act
        cat.setPrice(50.0);

        //Assert
        Assertions.assertEquals(50.0, cat.getPrice());
    }
}
