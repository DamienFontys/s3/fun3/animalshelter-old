package animalshelter.animals;

import animalshelter.enums.Gender;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class DogTests {

    @Test
    void DogConstructor_Works(){
        //Arrange

        //Act
        Dog dog = new Dog("Jan", Gender.Male);

        //Assert
        Assertions.assertEquals("Jan", dog.getName());
        Assertions.assertEquals(Gender.Male, dog.getGender());
    }

    @Test
    void getPrice_OnANewDog_ReturnsZero(){
        //Arrange
        Dog dog = new Dog("Jan", Gender.Male);

        //Act
        double price = dog.getPrice();

        //Assert
        Assertions.assertEquals(0, price);
    }

    @Test
    void getPrice_AfterSettingThePrice_OnADog_ReturnsTheNewPrice(){
        //Arrange
        Dog dog = new Dog("Jan", Gender.Male);

        //Act
        dog.setPrice(50.0);

        //Assert
        Assertions.assertEquals(50.0, dog.getPrice());
    }

}
