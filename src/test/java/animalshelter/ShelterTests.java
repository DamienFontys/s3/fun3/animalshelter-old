package animalshelter;

import animalshelter.animals.Cat;
import animalshelter.animals.Dog;
import animalshelter.enums.Gender;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;

import static animalshelter.enums.Species.Dog;

class ShelterTests {

    @Test
    void CreatingAShelter_InitializesAnEmptyAnimalsList() {
        //Arrange
        Shelter shelter = new Shelter();

        //Act

        //Assert
        Assertions.assertEquals(0, shelter.getAnimals().size());
    }

    @Test
    void Dogcount_OnAnInitializedShelter_ReturnsZero() {
        //Arrange
        Shelter shelter = new Shelter();

        //Act

        //Assert
        Assertions.assertEquals(0, shelter.DogCount());
    }

    @Test
    void Dogcount_AfterAddingACatToAnEmptyShelter_ReturnsZero() {
        //Arrange
        Shelter shelter = new Shelter();

        //Act
        shelter.addAnimal(new Cat("Cat", Gender.Male, ""));

        //Assert
        Assertions.assertEquals(0, shelter.DogCount());
    }

    @Test
    void Dogcount_AfterAddingADogToAnEmptyShelter_ReturnsOne(){
        //Arrange
        Shelter shelter = new Shelter();

        //Act
        shelter.addAnimal(new Dog("Dog", Gender.Male));

        //Assert
        Assertions.assertEquals(1, shelter.DogCount());
    }

    @Test
    void calculateAnimalPrice_ForTheSecondDog_UpdatesThePriceTo500(){
        //Arrange
        Shelter shelter = new Shelter();
        Dog dog = new Dog("Jan", Gender.Male);

        //Act
        shelter.addAnimal(dog);

        //Assert
        Assertions.assertEquals(500, dog.getPrice());
    }

    @Test
    void calculateAnimalPrice_ForTheSecondDog_UpdatesThePriceTo450(){
        //Arrange
        Shelter shelter = new Shelter();
        Dog dog = new Dog("Jan", Gender.Male);
        Dog dog2 = new Dog("Jan", Gender.Male);

        //Act
        shelter.addAnimal(dog);
        shelter.addAnimal(dog2);

        //Assert
        Assertions.assertEquals(450, dog2.getPrice());
    }

    @Test
    void calculateAnimalPrice_AfterManyDogsHaveBeenAdded_ReturnsTheMinPrice(){
        //Arrange
        Shelter shelter = new Shelter();

        for (int i = 0; i < 10; i++){
            shelter.addAnimal(new Dog("Jan", Gender.Male));
        }

        //Act
        Dog dog = new Dog("Jan", Gender.Male);
        shelter.addAnimal(dog);

        //Assert
        Assertions.assertEquals(50, dog.getPrice());
    }

    @Test
    void calculateAnimalPrice_ForACatWith0CharacterBadHabits_UpdatesThePriceTo350(){
        //Arrange
        Shelter shelter = new Shelter();
        Cat cat = new Cat("Jan", Gender.Male, "");

        //Act
        shelter.addAnimal(cat);

        //Assert
        Assertions.assertEquals(350, cat.getPrice());
    }

    @Test
    void calculateAnimalPrice_ForACatWith1CharacterBadHabits_UpdatesThePriceTo320(){
        //Arrange
        Shelter shelter = new Shelter();
        Cat cat = new Cat("Jan", Gender.Male, "1");

        //Act
        shelter.addAnimal(cat);

        //Assert
        Assertions.assertEquals(330, cat.getPrice());
    }

    @Test
    void calculateAnimalPrice_ForACatWith15PlusCharacterBadHabits_UpdatesThePriceToMinPrice(){
        //Arrange
        Shelter shelter = new Shelter();
        Cat cat = new Cat("Jan", Gender.Male, "This is a very bad cat");

        //Act
        shelter.addAnimal(cat);

        //Assert
        Assertions.assertEquals(35, cat.getPrice());
    }
}
