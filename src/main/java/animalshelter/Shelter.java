package animalshelter;

import animalshelter.animals.Animal;
import animalshelter.animals.Cat;
import animalshelter.animals.Dog;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Shelter {
    private List<Animal> Animals = new ArrayList<Animal>();
    private static double MaxCatPrice = 350;
    private static double MinCatPrice = 35;
    private static double MaxDogPrice = 500;
    private static double MinDogPrice = 50;

    void addAnimal(Animal animal) {
        calculateAnimalPrice(animal);
        this.Animals.add(animal);
    }

    List<Animal> getAnimals() {
        return Animals;
    }

    int DogCount() {
        return (int) this.Animals.stream().filter(animal -> animal.getClass() == Dog.class).count();
    }

    private double calculateCatPrice(Cat cat) {
        double price = MaxCatPrice;

        for (int i = 0; i < cat.getBadHabits().length(); i++) {
            price -= 20;
        }

        return Math.max(price, MinCatPrice);
    }

    private double calculateDogPrice(Dog dog) {
        double price = MaxDogPrice;

        for (int i = 0; i < DogCount(); i++) {
            price -= 50;
        }

        return Math.max(price, MinDogPrice);
    }

    double calculateAnimalPrice(Animal animal) {
        double price = 0;
        if (animal.getClass().equals(Dog.class)) {
            price = calculateDogPrice((Dog)animal);
        } else if (animal.getClass().equals(Cat.class)) {
            price = calculateCatPrice((Cat)animal);
        }

        animal.setPrice(price);
        return price;
    }
}
