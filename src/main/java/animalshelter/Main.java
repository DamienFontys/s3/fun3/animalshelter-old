package animalshelter;

import animalshelter.animals.Animal;
import animalshelter.animals.Cat;
import animalshelter.animals.Dog;
import animalshelter.enums.Gender;

import java.io.IOException;
import java.util.List;

public class Main {
    public static void main(String args[]) throws IOException {
        Shelter shelter = new Shelter();
        Marketplace marketplace = new Marketplace();
        Menu menu = new Menu(shelter);

        Dog dog = new Dog("Jane", Gender.Male);
        Dog dog2 = new Dog("Jane", Gender.Male);
        shelter.addAnimal(dog);
        shelter.addAnimal(dog2);

        List<Animal> animals = shelter.getAnimals();

        menu.showMenu();
    }

}
