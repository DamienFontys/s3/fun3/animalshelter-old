package animalshelter.interfaces;

public interface ISellable {
    double getPrice();
    String getName();
    void setPrice(double price);
}
