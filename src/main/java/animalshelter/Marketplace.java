package animalshelter;

import animalshelter.interfaces.ISellable;

import java.util.ArrayList;
import java.util.List;

public class Marketplace {
    private List<ISellable> marketplaceItems = new ArrayList<ISellable>();

    public List<ISellable> getMarketplaceItems() {
        return marketplaceItems;
    }

    public void addMarketplaceItem(ISellable item) {
        marketplaceItems.add(item);
    }

    public void removeMarketplaceItem(ISellable item) {
        marketplaceItems.remove(item);
    }
}
