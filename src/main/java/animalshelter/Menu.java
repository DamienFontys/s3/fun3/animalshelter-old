package animalshelter;

import animalshelter.animals.Animal;
import animalshelter.animals.Cat;
import animalshelter.animals.Dog;
import animalshelter.enums.Gender;
import animalshelter.enums.Species;
import jdk.nashorn.internal.ir.annotations.Ignore;

import java.io.IOException;
import java.util.List;
import java.util.Scanner;

@Ignore
public class Menu {
    private Shelter shelter;

    public Menu(Shelter shelter) {
        this.shelter = shelter;
    }

    public void showMenu() throws IOException {
        System.out.println("------  Animal Shelter Menu ------");
        System.out.println("");
        System.out.println("[1] Show Animals");
        System.out.println("[2] Add Animal");
        System.out.println("");
        System.out.println("------  Animal Shelter Menu ------");

        String input = new Scanner(System.in).next();

        if (input.equals("1")) {
            showAnimals();
            System.out.println("");
            showMenu();
        } else if (input.equals("2")) {
            showAddAnimal();
        } else {
            showMenu();
        }
    }

    private void showAnimals() throws IOException {
        List<Animal> animals = shelter.getAnimals();
        for (Animal animal : animals) {
            System.out.println(String.format("%s, %s;", animal.getName(), animal.getGender()));
        }
        System.out.println("");
        System.out.println("Press Enter to continue...");
        System.in.read();
    }

    private void showAddAnimal() throws IOException {
        Species species = null;
        String name = null;
        Gender gender = null;
        Scanner scan = new Scanner(System.in);

        while (species == null) {
            System.out.println("Species:");
            System.out.println("[1] Cat");
            System.out.println("[2] Dog");
            String input = scan.next();

            if (input.equals("1")) {
                species = Species.Cat;
            } else if (input.equals("2")) {
                species = Species.Dog;
            }
        }

        while (name == null) {
            System.out.println("Name:");
            String input = scan.next();
            if (!input.equals("")) {
                name = input;
            }
        }

        while (gender == null) {
            System.out.println("Gender:");
            System.out.println("[1] Male");
            System.out.println("[2] Female");
            String input = scan.next();

            if (input.equals("1")) {
                gender = Gender.Male;
            } else if (input.equals("2")) {
                gender = Gender.Female;
            }
        }

        Animal animal;
        if (species == Species.Cat) {
            System.out.println("Bad Habits:");
            String input = scan.next();
            animal = new Cat(name, gender, input);
        } else {
            animal = new Dog(name, gender);
        }

        this.shelter.addAnimal(animal);
        showMenu();
    }
}
