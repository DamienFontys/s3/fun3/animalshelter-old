package animalshelter.animals;

import animalshelter.interfaces.ISellable;

public class Dog extends Animal {
    private double price;
    public Dog(String name, animalshelter.enums.Gender gender) {
        super(name, gender);
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price){
        this.price = price;
    }
}
