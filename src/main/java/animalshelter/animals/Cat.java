package animalshelter.animals;

import animalshelter.interfaces.ISellable;

public class Cat extends Animal {
    private String badHabits = null;
    private double price = 0;
    public Cat(String name, animalshelter.enums.Gender gender, String badHabits) {
        super(name, gender);
        this.badHabits = badHabits;
    }

    public String getBadHabits() {
        return badHabits;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price){
        this.price = price;
    }
}
