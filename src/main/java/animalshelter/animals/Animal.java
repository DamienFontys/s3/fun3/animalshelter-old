package animalshelter.animals;

import animalshelter.enums.Gender;
import animalshelter.interfaces.ISellable;

public abstract class Animal implements ISellable{
    private String Name;
    private animalshelter.enums.Gender Gender;

    Animal(String name, Gender gender) {
        this.Name = name;
        this.Gender = gender;
    }

    public String getName() {
        return Name;
    }

    public Gender getGender() {
        return Gender;
    }


}
